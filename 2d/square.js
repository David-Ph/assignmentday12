const TwoDimension = require("./twoDimension");

class Square extends TwoDimension {
  constructor(length) {
    super("Square");
    this.length = length;
  }

  calculateCircumference() {
    return 4 * this.length;
  }

  calculateArea() {
    return this.length * this.length;
  }
}

module.exports = Square;
