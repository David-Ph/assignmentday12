// David
const TwoDimension = require("./twoDimension");

class Rectangle extends TwoDimension {
  constructor(width, length) {
    super("Rectangle");
    this.width = width;
    this.length = length;
  }

  calculateCircumference() {
    return 2 * (this.width + this.length);
  }

  calculateArea() {
    return this.width * this.length;
  }
}

module.exports = Rectangle;
