const Geometry = require("../geometry");

class TwoDimension extends Geometry {
  constructor(name) {
    super(name, "Two Dimension");
    if (this.constructor === TwoDimension) {
      throw new Error("Two Dimension is a abstract class");
    }
  }
}

module.exports = TwoDimension;
