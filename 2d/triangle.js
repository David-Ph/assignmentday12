//ruhul

const TwoDimension = require("./twoDimension");

class Triangle extends TwoDimension {
  constructor(base, height) {
    super("Triangle");
    this.base = base;
    this.height = height;
  }

  calculateCircumference() {
    return this.base + this.base + this.base;
  }

  calculateArea() {
    return 0.5 * (this.base * this.height);
  }
}

module.exports = Triangle;
