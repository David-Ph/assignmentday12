class Geometry {
  constructor(name, type) {
    if (this.constructor === Geometry) {
      throw new Error("Geometry is a abstract class");
    }

    this.name = name;
    this.type = type;
  }
}

module.exports = Geometry;
