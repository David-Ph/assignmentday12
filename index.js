const Square = require("./2d/square");
const Rectangle = require("./2d/rectangle");
const Tube = require("./3d/tube");
const Beam = require("./3d/beam");
const Cube = require("./3d/cube");
const Triangle = require("./2d/triangle");
const Cone = require("./3d/cone");

const square1 = new Square(10);
square1Circ = square1.calculateCircumference();
square1Area = square1.calculateArea();

console.log(`Square 1 circumference is ${square1Circ}`);
console.log(`Square 1 area is ${square1Area}`);

console.log("===================================================");

const rectangle1 = new Rectangle(15, 7);
rectangle1Circ = rectangle1.calculateCircumference();
rectangle1Area = rectangle1.calculateArea();

console.log(`Rectangle 1 circumference is ${rectangle1Circ}`);
console.log(`Rectangle 1 area is ${rectangle1Area}`);

console.log("===================================================");

const tube1 = new Tube(10, 5);
tube1Volume = tube1.calculateVolume();
tube1Area = tube1.calculateArea();

console.log(`Tube 1 volume is ${tube1Volume}`);
console.log(`Tube 1 Area is ${tube1Area}`);

console.log("===================================================");

const beam1 = new Beam(12, 10, 10);
beam1Volume = beam1.calculateVolume();
beam1Area = beam1.calculateArea();

console.log(`Beam 1 Volume is ${beam1Volume}`);
console.log(`Beam 1 area is ${beam1Area}`);

console.log("===================================================");

const cube1 = new Cube(10);
cube1Volume = cube1.calculateVolume();
cube1Area = cube1.calculateArea();

console.log(`Cube 1 Volume is ${cube1Volume}`);
console.log(`Cube 1 area is ${cube1Area}`);
console.log("===================================================");

const Triangle1 = new Triangle(12, 10);
Triangle1Circ = Triangle1.calculateCircumference();
Triangle1Area = Triangle1.calculateArea();

console.log(`Triangle 1 circumference is ${Triangle1Circ}`);
console.log(`Triangle 1 area is ${Triangle1Area}`);

console.log("===================================================");

const cone1 = new Cone(14, 27, 31);
cone1Volume = cone1.calculateVolume();
cone1Area = cone1.calculateArea();

console.log(`Cone 1 area is ${cone1Area}`);
console.log(`cone 1 volume is ${cone1Volume}`);
