// David
const ThreeDimension = require("./threeDimension");

class Tube extends ThreeDimension {
  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
  }
  static PI = 3.14;

  calculateArea() {
    const circlesArea = parseFloat(
      2 * (Tube.PI * (this.radius * this.radius)).toFixed(2)
    );
    const blankedArea = parseFloat(
      (2 * (Tube.PI * this.radius * this.height)).toFixed(2)
    );
    return circlesArea + blankedArea;
  }

  calculateVolume() {
    return parseFloat(
      (Tube.PI * (this.radius * this.radius) * this.height).toFixed(2)
    );
  }
}

module.exports = Tube;
