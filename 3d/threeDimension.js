const Geometry = require("../geometry");

class ThreeDimension extends Geometry {
  constructor(name) {
    super(name, "Three Dimension");
    if (this.constructor === ThreeDimension) {
      throw new Error("Three Dimention is a abstract class");
    }
  }
}

module.exports = ThreeDimension;
