const ThreeDimension = require("./threeDimension");

//author : rezki
class Beam extends ThreeDimension {
  constructor(length, width, height) {
    super("Beam");
    this.length = length;
    this.width = width;
    this.height = height;
  }
  calculateVolume() {
    return this.length * this.width * this.height;
  }

  calculateArea() {
    return (
      2 *
      (this.length * this.width +
        this.length * this.height +
        this.width * this.height)
    );
  }
}

module.exports = Beam;
